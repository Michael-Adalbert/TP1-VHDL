 #!/bin/sh

if [ $# -ne 2 ]
	then
	echo "usage : $0 <file.vhd> <testbench.vhd>"
	exit 1
fi
#file name
FILE=$1
#testbench name
TESTBENCH=$2
#test entity name
VAR=`grep "^entity" $TESTBENCH`
NAME=${VAR%' is'}
NAME=${NAME##'entity '}

ghdl -a --ieee=synopsys -fexplicit $FILE

ghdl -a --ieee=synopsys -fexplicit $TESTBENCH

ghdl -e --ieee=synopsys -fexplicit $NAME 

ghdl -r --ieee=synopsys -fexplicit $NAME --wave=$NAME.ghw


